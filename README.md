# CrowdSec Forward Auth Server

A small Rust HTTP API that will validate HTTP request IP addresses against a crowdsec
local API.

## Getting Started

First, install redis

```
helm upgrade --install --namespace <namespace> \
    crowdsec-redis bitnami/redis \
    --set replica.replicaCount=0 \
    --set global.storageClass="<storageClass>"
```

Then, install the helm chart in this repo with appropriate redis settings in the values

```
helm upgrade --install --namespace <namespace> \
    development ./chart \
    -f custom-values.yaml
```

Then, setup a Traefik middleware to point to the auth server

```
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: crowdsec
spec:
  forwardAuth:
    address: http://<service>.<namespace>.svc.cluster.local:<port>/

```
