use axum::{
    debug_handler, extract::State, http::StatusCode, response::IntoResponse, response::Response,
};

use axum_client_ip::InsecureClientIp;
use bb8_redis::redis::AsyncCommands;
use tracing::info;

use crowdsec::fetch_decisions;

use crate::crowdsec;
use crate::settings::Settings;
use crate::types::RedisConnectionPool;

/// Utility function for mapping any error into a `500 Internal Server Error`
/// response.
fn internal_error<E>(err: E) -> (StatusCode, String)
where
    E: std::error::Error,
{
    (StatusCode::INTERNAL_SERVER_ERROR, err.to_string())
}

#[debug_handler]
pub async fn authenticate(
    State(pool): State<RedisConnectionPool>,
    insecure_ip: InsecureClientIp,
) -> Result<Response, (StatusCode, String)> {
    // Get the end client's ip address
    let raw_ip = insecure_ip.0.to_string();
    let redis_key = raw_ip.to_owned();

    info!("Attempting to verify {}", raw_ip);

    let settings = Settings::new().map_err(internal_error)?;

    // Get a connection to redis and check if we have a cached vesion already
    let mut conn = pool.get().await.map_err(internal_error)?;
    let response: Option<String> = conn.get(&redis_key).await.map_err(internal_error)?;
    if let Some(result) = response {
        // If there is a cached response, use that
        if result == "ban" {
            return Err((StatusCode::FORBIDDEN, "Banned".to_string()));
        }
        return Ok(result.into_response());
    }

    // If we don't have a response in redis, then check crowdsec explicitly
    let client = reqwest::Client::new();
    let decisions_response = fetch_decisions(
        &client,
        format!("{}://{}", settings.crowdsec_scheme, settings.crowdsec_host),
        settings.crowdsec_api_key,
        raw_ip,
    )
    .await
    .map_err(|e| (StatusCode::INTERNAL_SERVER_ERROR, e.to_string()))?;

    // We need to cache for this web request - so let's set up a placeholder value to "allow"
    let mut redis_value: String = String::from("ok");
    if let Some(decisions) = decisions_response {
        if let Some(decision) = decisions.first() {
            redis_value = decision.decision_type.clone();
        }
    }

    // Set the new redis cache value for this ip address
    let _: () = conn
        .set_ex(&redis_key, &redis_value, settings.redis_timeout)
        .await
        .map_err(internal_error)?;

    // Return our response from the web server
    if redis_value == "ban" {
        return Err((StatusCode::FORBIDDEN, "Banned".to_string()));
    }
    Ok(redis_value.into_response())
}

#[debug_handler]
pub async fn healthcheck() -> Result<Response, (StatusCode, String)> {
    Ok("ok".into_response())
}
