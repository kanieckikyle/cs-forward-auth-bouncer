use axum::{
    error_handling::HandleErrorLayer, http::StatusCode, response::IntoResponse, routing::get,
    Router,
};

use bb8_redis::{bb8, RedisConnectionManager};

use axum_client_ip::SecureClientIpSource;
use std::{borrow::Cow, net::SocketAddr, time::Duration};
use tower::{BoxError, ServiceBuilder};
use tower_http::trace::TraceLayer;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

mod crowdsec;
mod routes;
mod settings;
mod types;

use routes::{authenticate, healthcheck};
use settings::Settings;

#[tokio::main]
async fn main() {
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env()
                .unwrap_or_else(|_| "tower_http=debug".into()),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();

    let settings = Settings::new().unwrap();

    let manager = RedisConnectionManager::new(format!(
        "redis://{}:{}@{}:{}/{}",
        settings.redis_username,
        settings.redis_password,
        settings.redis_host,
        settings.redis_port,
        settings.redis_db
    ))
    .unwrap();
    let pool = bb8::Pool::builder().build(manager).await.unwrap();

    // Build our application by composing routes
    let app = Router::new()
        .route("/", get(authenticate))
        .route("/healthz", get(healthcheck))
        // Add middleware to all routes
        .layer(
            ServiceBuilder::new()
                // Handle errors from middleware
                .layer(HandleErrorLayer::new(handle_error))
                .load_shed()
                .concurrency_limit(1024)
                .timeout(Duration::from_secs(10))
                .layer(TraceLayer::new_for_http()),
        )
        .layer(SecureClientIpSource::ConnectInfo.into_extension())
        .with_state(pool);

    // Run our app with hyper
    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service_with_connect_info::<SocketAddr>())
        .await
        .unwrap();
}

async fn handle_error(error: BoxError) -> impl IntoResponse {
    if error.is::<tower::timeout::error::Elapsed>() {
        return (StatusCode::REQUEST_TIMEOUT, Cow::from("request timed out"));
    }

    if error.is::<tower::load_shed::error::Overloaded>() {
        return (
            StatusCode::SERVICE_UNAVAILABLE,
            Cow::from("service is overloaded, try again later"),
        );
    }

    (
        StatusCode::INTERNAL_SERVER_ERROR,
        Cow::from(format!("Unhandled internal error: {}", error)),
    )
}
