use serde_derive::{Deserialize, Serialize};
use serde_json::json;
use tower::BoxError;
use tracing::info;

#[derive(Debug, Serialize, Deserialize)]
pub struct Decision {
    pub id: u32,
    pub origin: String,
    pub duration: String,
    pub scenario: String,
    pub scope: String,
    pub value: String,

    #[serde(rename = "type")]
    pub decision_type: String,
}

pub async fn fetch_decisions(
    client: &reqwest::Client,
    base_url: String,
    token: String,
    ip: String,
) -> Result<Option<Vec<Decision>>, BoxError> {
    info!("Fetching decisions from {}", "crowdsec.cytra.co");
    let response = client
        .get(format!("{}/v1/decisions", base_url))
        .query(&json!({
            "ip": ip
        }))
        .header("X-Api-Key", token)
        .header("User-Agent", "Crowdsec Forward Auth Bouncer")
        .send()
        .await?;

    Ok(match response.json::<Vec<Decision>>().await {
        Ok(decisions) => Some(decisions),
        _ => None,
    })
}
