use config::{Config, ConfigError, Environment};
use serde_derive::Deserialize;

fn default_crowdsec_scheme() -> String {
    "https".to_string()
}

fn default_redis_port() -> u64 {
    6379
}

fn default_redis_db() -> u64 {
    0
}

fn default_redis_username_password() -> String {
    "".to_string()
}

fn default_redis_timeout() -> usize {
    60
}

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct Settings {
    pub crowdsec_host: String,

    pub crowdsec_api_key: String,

    #[serde(default = "default_crowdsec_scheme")]
    pub crowdsec_scheme: String,

    pub redis_host: String,

    #[serde(default = "default_redis_port")]
    pub redis_port: u64,

    #[serde(default = "default_redis_db")]
    pub redis_db: u64,

    #[serde(default = "default_redis_username_password")]
    pub redis_username: String,

    #[serde(default = "default_redis_username_password")]
    pub redis_password: String,

    #[serde(default = "default_redis_timeout")]
    pub redis_timeout: usize,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let s = Config::builder()
            .add_source(Environment::default())
            .build()?;

        // You can deserialize (and thus freeze) the entire configuration as
        s.try_deserialize()
    }
}
