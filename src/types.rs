use bb8_redis::{bb8, RedisConnectionManager};

pub type RedisConnectionPool = bb8::Pool<RedisConnectionManager>;
